module gitee.com/klogsdk/klog-go-sdk

go 1.17

require (
	github.com/aws/aws-sdk-go v1.42.5
	github.com/golang/protobuf v1.5.2
	github.com/pierrec/lz4/v4 v4.1.14
	github.com/stretchr/testify v1.7.0
	google.golang.org/protobuf v1.28.0
)

require (
	github.com/bwmarrin/snowflake v0.3.0 // indirect
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
