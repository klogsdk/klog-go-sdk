# 金山云日志服务(KLog) SDK for golang使用指南

+ [金山云日志服务产品简介](https://www.ksyun.com/nv/product/KLog.html)
+ [金山云日志服务产品文档](https://docs.ksyun.com/products/123)

## 安装

使用go get 安装

```shell
go get gitee.com/klogsdk/klog-go-sdk
```

## 使用方法

### 初始化KLog客户端

KLog客户端是线程安全的。在整个进程内您可以只创建一个KLog客户端，并重复使用。

```golang
import (
   "gitee.com/klogsdk/klog-go-sdk/klog"
   "time"
)

func main() {
    // 您在金山云的主账户或子账户的 ACCESS KEY ID
    accessKey = "your secret_key"
    
    // 您在金山云的主账户或子账户的 SECRET KEY ID
    secretKey = "your secret_key"
    
    // 您的日志项目所在地区的入口地址，该地址可以在金山云控制台日志服务的项目概览中查到。
    // 支持 http 和 https
    endpoint = "https://klog-cn-beijing.ksyun.com"
    
    // 创建KLog客户端
    client := klog.NewClient(&klog.ClientOptions{
        AccessKey: accessKey,
        SecretKey: secretKey,
        Endpoint:  endpoint,
    })
}
```


### 上传文本类型日志

```golang
// 异步发送一条文本日志
err := client.PushString("your_project_name", "your_pool_name", "log_source_name", "log_file_path", "hahaha1", time.Now().UnixMilli())
```

### 上传多字段日志

KLog支持protobuf类型的日志。注意：同一日志池各条日志的数据结构应该保持一致。

```golang
// 异步发送一条protobuf日志
pbLog := &klog.Log{Time: time.Now().UnixMilli()}
pbLog.Contents = append(pbLog.Contents, &klog.Log_Content{
    Key:   "key1",
    Value: "hahaha4",
})
pbLog.Contents = append(pbLog.Contents, &klog.Log_Content{
    Key:   "key2",
    Value: "hahaha5",
})
err := client.Push("your_project_name", "your_pool_name", "log_source_name", "log_file_path", pbLog)
```

### 异步发送

KLog客户端默认是异步发送数据的，客户端内部的发送间隔为每2秒，或每批达到3MB，或每批达到4096条。 这样的好处有：

+ 客户端内部自动将最近的多条日志一起压缩并批量发送。
+ 不会阻塞其它逻辑(除非发送缓冲队列满了)
+ 可以配置各种发送策略

注意：

+ 程序退出时，需调用一次`Client.Flush()`。

```golang
// 立即发送客户端缓冲队列中还未发送的日志。
client.Flush(true)
```

### 同步发送

+ 在调用`Client.Push()`之后调用`Client.Flush()`，可实现同步发送。但通常不建议这么做。

## 参数说明

### klog.ClientOptions

创建一个KLog客户端初始化选项，其参数如下：

+ `Endpoint` 必填。您的日志项目所在地区的入口地址，该地址可以在金山云控制台日志服务的项目概览中查到。支持`http`和`https`。
+ `Credentials` 选填。此项为空时，`AccessKey`和`SecretKey`不可为空。
+ `AccessKey` 选填。您在金山云的主账户或子账户的`ACCESS KEY ID`。
+ `SecretKey` 选填。您在金山云的主账户或子账户的`SECRET KEY ID`。
+ `RateLimit` 限制发送速率为每秒多少条。此项配置可降低CPU使用率，但会降低发送速率，在日志较多时，缓冲队列可能会满。默认为0，即不限制。
+ `DownSampleRate` 降采样率。例如设置为0.15时，将只发送15%的日志，其余丢弃。此项配置可降低CPU使用率。默认为1，即发送所有日志。
+ `MaxRetries` 发送失败后的重试次数，达到次数后如果仍然失败则丢弃日志。
+ `Logger` 设置客户端输出自身运行状态的日志对象。默认为打印到stdout。
+ `LogLevel` 客户端内部日志打印level。默认为service.LevelInfo。
+ `HTTPClient` 客户端所使用的http客户端。
+ `DropIfPoolNotExists` 如果服务器返回日志池不存在，是否丢弃日志。默认为`false`，即不丢弃。

### Client.Push(projectName, logPoolName, source, filename string, log *pb.Log) error

上传一条日志。参数如下：

+ `projectName` 必填。项目名称
+ `logPoolName` 必填。日志池名称
+ `source` 日志来源，如主机名、ip等，用于进行日志上下文查询
+ `filename` 日志文件路径，用于进行日志上下文查询
+ `log` 必填。日志数据，protobuf类型。


### Client.PushString(projectName, logPoolName, message, source, filename string, timestamp int64) error

上传一条文本日志。参数如下：

+ `projectName` 必填。项目名称
+ `logPoolName` 必填。日志池名称
+ `message` 必填。日志数据，字符串类型。
+ `source` 日志来源，如主机名、ip等，用于进行日志上下文查询
+ `filename` 日志文件路径，用于进行日志上下文查询
+ `timestamp` 日志时间戳，UNIX毫秒值。

### Client.Flush(wait)

立即发送客户端缓冲队列中还未发送的日志。
+ `wait` 必填。是否等待发送结束

### Client.GetStat()
获取发送统计

### Client.GetStatDelta()
获取发送统计距离上次调用GetStatDelta()的增量，
