package klog

const (
	MaxKeyCount     = 900     // amount
	MaxKeySize      = 1 << 20 // byte
	MaxValueSize    = 1 << 20 // byte
	MaxBulkSize     = 4 << 10 // amount
	MaxLogSize      = 3000000 // byte
	MaxLogGroupSize = 3000000 // byte
)

// 错误码
const (
	InternalServerError              = "InternalServerError"
	SignatureNotMatch                = "SignatureNotMatch"
	PostBodyTooLarge                 = "PostBodyTooLarge"
	PostBodyInvalid                  = "PostBodyInvalid" // deprecated
	ProjectOrLogPoolNotExist         = "ProjectOrLogPoolNotExist"
	UserNotExist                     = "UserNotExist"
	MaxBulkSizeExceeded              = "MaxBulkSizeExceeded"
	MaxKeyCountExceeded              = "MaxKeyCountExceeded"
	MaxKeySizeExceeded               = "MaxKeySizeExceeded"
	MaxValueSizeExceeded             = "MaxValueSizeExceeded"
	MaxLogSizeExceeded               = "MaxLogSizeExceeded"
	InvalidUtf8InKey                 = "InvalidUtf8InKey"
	InvalidUtf8InValue               = "InvalidUtf8InValue"
	HttpError                        = "HttpError"
	SignatureError                   = "SignatureError"
	CompressLz4Error                 = "CompressLz4Error"
	UnknownError                     = "UnknownError"
	RequestOpenAPIBodyOrParamInvalid = "RequestOpenAPIBodyOrParamInvalid"
	InvalidRequest                   = "InvalidRequest"
	JsonMarshalError                 = "JsonMarshalError"
	JsonUnMarshalError               = "JsonUnMarshalError"
)

const (
	HeaderRequestId            = "X-KSC-REQUEST-ID"
	OpenAPIVersion             = "2020-07-31"
	SDKInternalErrorStatusCode = -1
)

var supportIndexType = []string{"text", "long", "double", "date", "json"}
