package klog

import (
	"crypto/rand"
	"encoding/hex"
	"io"
)

type uUid [16]byte

var rander = rand.Reader

func newUUID() uUid {
	uuid, err := newRandomFromReader(rander)
	if err != nil {
		return uUid{}
	}
	return uuid
}

func newRandomFromReader(r io.Reader) (uUid, error) {
	var uuid uUid
	_, err := io.ReadFull(r, uuid[:])
	if err != nil {
		return uUid{}, err
	}
	uuid[6] = (uuid[6] & 0x0f) | 0x40
	uuid[8] = (uuid[8] & 0x3f) | 0x80
	return uuid, nil
}

func (uuid uUid) String() string {
	var buf [36]byte
	encodeHex(buf[:], uuid)
	return string(buf[:])
}

func encodeHex(dst []byte, uuid uUid) {
	hex.Encode(dst, uuid[:4])
	dst[8] = '-'
	hex.Encode(dst[9:13], uuid[4:6])
	dst[13] = '-'
	hex.Encode(dst[14:18], uuid[6:8])
	dst[18] = '-'
	hex.Encode(dst[19:23], uuid[8:10])
	dst[23] = '-'
	hex.Encode(dst[24:], uuid[10:])
}
