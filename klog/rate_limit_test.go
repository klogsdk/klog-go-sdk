package klog

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewRateLimit(t *testing.T) {
	var d *RateLimit
	a := assert.New(t)

	d = NewRateLimit(100, 10)
	a.Equal(100, d.limitPerSec)

	d = NewRateLimit(0, 10)
	a.Equal(1, d.limitPerSec)

	d = NewRateLimit(10, 100)
	a.Equal(10, d.slotsLen)

	d = NewRateLimit(10, -1)
	a.Equal(1, d.slotsLen)
}

func TestRateLimit_Wait(t *testing.T) {
	a := assert.New(t)
	mSec := duration(NewRateLimit(1, 10), 3)
	a.Greater(mSec, 2500)
	a.Greater(3500, mSec)

	mSec = duration(NewRateLimit(543, 10), 543*3)
	a.Greater(mSec, 2500)
	a.Greater(3500, mSec)
}

func duration(rateLimit *RateLimit, total int) int {
	t := time.Now().UnixMilli()
	for i := 0; i < total; i++ {
		rateLimit.Wait()
	}
	return int(time.Now().UnixMilli() - t)
}
