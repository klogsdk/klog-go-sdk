package klog

import (
	"bytes"
	"encoding/json"
	"gitee.com/klogsdk/klog-go-sdk/credentials"
	"github.com/stretchr/testify/assert"
	"net/http"
	"strconv"
	"testing"
	"time"
)

func TestSignatureV4(t *testing.T) {
	a := assert.New(t)
	m := map[string]string{}
	bs, _ := json.Marshal(m)
	r := bytes.NewReader(bs)
	req, _ := http.NewRequest("POST", "http://127.0.0.1:9999/PutLogsM", r)
	req.Header.Set("Content-Length", strconv.Itoa(len(bs)))
	req.Header.Set("Content-Type", "application/x-protobuf")
	req.Header.Set("x-klog-api-version", "0.1.0")
	req.Header.Set("x-klog-signature-method", "hmac-sha1")
	req.Header.Set("x-klog-compress-type", "lz4")
	req.Header.Set("Date", time.Now().UTC().Format(http.TimeFormat))
	var ak = ""
	var sk = ""
	sc := credentials.NewStaticCredentials(ak, sk, "")
	if err := signatureV4(sc, "inner", req, r); err != nil {
		t.Fatalf(err.Error())
	}
	for k, v := range req.Header {
		t.Logf("headers k: %s v: %s\n", k, v[0])
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer resp.Body.Close()
	respM := map[string]interface{}{}
	if err = json.NewDecoder(resp.Body).Decode(&respM); err != nil {
		t.Fatalf(err.Error())
	}
	authorized := respM["authorized"].(bool)
	a.Equal(true, authorized)
}
