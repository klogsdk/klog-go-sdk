package klog

type Client struct {
	*adminClient
	options *ClientOptions
	worker  *worker
	stat    *Stat
}

func NewClient(options *ClientOptions) *Client {
	options.init()
	stat := newStat()
	return &Client{
		options:     options,
		stat:        stat,
		worker:      newWorker(options, stat),
		adminClient: newAdminClient(options, stat),
	}
}

// Push 异步发送一条log
// 如果返回error，表示日志大小、编码等不符合要求
// source: 日志来源，如主机名、ip等，用于进行日志上下文查询
// filename: 日志文件路径，用于进行日志上下文查询
func (o *Client) Push(projectName, logPoolName, source, filename string, log *Log) error {
	return o.worker.push(projectName, logPoolName, source, filename, log)
}

// PushString 是 Push 的一个便利用法。
func (o *Client) PushString(projectName, logPoolName, message, source, filename string, timestamp int64) error {
	log := &Log{
		Time: timestamp,
		Contents: []*Log_Content{
			{Key: "message", Value: message},
		},
	}
	return o.Push(projectName, logPoolName, source, filename, log)
}

// Stop 用于程序退出
func (o *Client) Stop() {
	o.worker.stop()
}

// Flush 在程序结束的时候，需要调用一次该函数
// wait 是否等待发送完毕
func (o *Client) Flush(wait bool) {
	o.worker.flush(wait)
}

func (o *Client) GetStat() *Stat {
	return o.stat.getCurrent()
}

func (o *Client) GetStatDelta() *Stat {
	return o.stat.getDelta()
}
