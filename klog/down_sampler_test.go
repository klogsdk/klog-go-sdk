package klog

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewDownSampler(t *testing.T) {
	var d *DownSampler
	a := assert.New(t)

	d = NewDownSampler(-1)
	a.Equal(MINIMAL_RATE, d.rate)

	d = NewDownSampler(1.1)
	a.Equal(1.0, d.rate)

	d = NewDownSampler(0)
	a.Equal(MINIMAL_RATE, d.rate)
}

func TestDownSampler_Ok(t *testing.T) {
	a := assert.New(t)
	a.Equal(1, count(1, 0.001))
	a.Equal(1000, count(1000, 1))
	a.Equal(5, count(10, 0.5))
	a.Equal(342, count(98765, 0.0034567))
	a.Equal(777, count(1234, 0.63))
	a.Equal(200000, count(2000000, 0.1))
}

func count(n int, rate float64) int {
	d := NewDownSampler(rate)
	total := 0
	for i := 0; i < n; i++ {
		if d.Ok() {
			total++
		}
	}
	return total
}
