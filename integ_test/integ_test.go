package integ

import (
	"fmt"
	"gitee.com/klogsdk/klog-go-sdk/klog"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var endpoint = "http://127.0.0.1:8765"

func init() {
	newMockServer()
}

func TestMain(m *testing.M) {
	server.start()
	m.Run()
	server.stop()
}

func Test_MockServer(t *testing.T) {
	server.clearStat()
	_, _ = server.doRequest("/PutLogsM")
	a := assert.New(t)
	stat := server.getPoolStat("", "")
	a.Equal(1, stat.Requests)
	a.Equal(1, stat.C400)
	server.clearStat()
}

func Test_Send(t *testing.T) {
	server.clearStat()
	a := assert.New(t)
	p := "p"
	l := "l"
	client := klog.NewClient(&klog.ClientOptions{AccessKey: "ak", SecretKey: "sk", Endpoint: endpoint})

	client.PushString(p, l, "message", "", "", time.Now().UnixMilli())
	time.Sleep(time.Duration(2050) * time.Millisecond)
	stat := server.getPoolStat(p, l)
	a.Equal(1, stat.Batches)
	a.Equal(1, stat.Logs)
	a.Equal(1, stat.Contents)

	stat = server.getPoolStat("", "")
	a.Equal(1, stat.Requests)
	a.Equal(1, stat.C200)
	a.Equal(0, stat.C400)
	a.Equal(0, stat.C500)

	client.PushString(p, l, "message", "a", "b", time.Now().UnixMilli())
	client.PushString(p, l, "message", "a", "b", time.Now().UnixMilli())
	client.Flush(true)
	stat = server.getPoolStat(p, l)
	a.Equal(2, stat.Batches)
	a.Equal(3, stat.Logs)
	a.Equal(3, stat.Contents)

	stat = server.getPoolStat("", "")
	a.Equal(2, stat.Requests)
	a.Equal(2, stat.C200)
	a.Equal(0, stat.C400)
	a.Equal(0, stat.C500)

	client.PushString(p, l, "message", "a", "b", time.Now().UnixMilli())
	client.PushString(p, l, "message", "a", "c", time.Now().UnixMilli())
	client.PushString(p, l, "message", "a", "c", time.Now().UnixMilli())
	client.Flush(true)
	stat = server.getPoolStat(p, l)
	a.Equal(4, stat.Batches)
	a.Equal(6, stat.Logs)
	a.Equal(6, stat.Contents)
}

func Test_BatchSend(t *testing.T) {
	server.clearStat()
	a := assert.New(t)
	p := "p"
	l := "l"
	n := 12000
	client := klog.NewClient(&klog.ClientOptions{AccessKey: "ak", SecretKey: "sk", Endpoint: endpoint})
	for i := 0; i < n; i++ {
		client.PushString(p, l, "message", "", "", time.Now().UnixMilli())
	}

	client.Flush(true)
	stat := server.getPoolStat(p, l)
	a.Equal(3, stat.Batches)
	a.Equal(n, stat.Logs)
	a.Equal(n, stat.Contents)

	stat = server.getPoolStat("", "")
	a.Equal(3, stat.Requests)
	a.Equal(3, stat.C200)
	a.Equal(0, stat.C400)
	a.Equal(0, stat.C500)

}

func Test_InternalServerErrorAndMaxRetries(t *testing.T) {
	server.clearStat()
	a := assert.New(t)
	p := "p"
	l := "l"
	client := klog.NewClient(&klog.ClientOptions{
		AccessKey:  "ak",
		SecretKey:  "sk",
		Endpoint:   endpoint,
		MaxRetries: 4,
	})
	client.PushString(p, l, "message", "", "", time.Now().UnixMilli())
	server.setModeInternalServerError()
	client.Flush(true)
	stat := server.getPoolStat("", "")
	a.Equal(5, stat.Requests)
	a.Equal(5, stat.C500)

	// test client stat
	clientStat := client.GetStat()
	a.Equal(4, int(clientStat.Retried))
	a.Equal(1, int(clientStat.FailedBatches))
	a.Equal(1, int(clientStat.FailedLogs))
}

func TestFlush(t *testing.T) {
	server.clearStat()
	a := assert.New(t)
	p := "p"
	l := "l"
	n := 200000

	client := klog.NewClient(&klog.ClientOptions{
		AccessKey: "ak",
		SecretKey: "sk",
		Endpoint:  endpoint,
		WorkerNum: 5,
	})
	for i := 0; i < n; i++ {
		if i%5000 == 0 {
			client.Flush(true)
		}
		client.PushString(p, l, "message", "", "", time.Now().UnixMilli())
	}

	client.Flush(true)

	// client stat
	clientStat := client.GetStat()

	// server stat
	serverStat := server.getPoolStat(p, l)
	a.Equal(n, serverStat.Logs)
	a.Equal(n, serverStat.Contents)
	a.Equal(serverStat.Logs, int(clientStat.SentLogs))
	a.Equal(serverStat.Batches, int(clientStat.SentBatches))
	a.Equal(0, int(clientStat.Retried))
}

func Test_InternalServerErrorAndRecover(t *testing.T) {
	server.clearStat()
	a := assert.New(t)
	p := "p"
	l := "l"
	n := 4000
	client := klog.NewClient(&klog.ClientOptions{
		AccessKey:  "ak",
		SecretKey:  "sk",
		Endpoint:   endpoint,
		MaxRetries: 100,
	})

	mode500 := false
	for i := 0; i < n; i++ {
		if i%200 == 0 {
			if mode500 {
				server.setModeInternalServerError()
			} else {
				server.setModeNormal()
			}
			mode500 = !mode500
			client.Flush(false)
		}
		client.PushString(p, l, "message", "", "", time.Now().UnixMilli())
		time.Sleep(time.Millisecond * time.Duration(1))
	}
	server.setModeNormal()

	client.Flush(true)

	// client stat
	clientStat := client.GetStat()
	now := time.Now().In(klog.BeijingZone).Format("2006-01-02T15:04:05.000+0800")
	a.Equal(0, int(clientStat.DownSampledLogs))
	a.Equal(0, int(clientStat.DroppedLogs))
	a.Equal(0, int(clientStat.ErroredLogs))
	a.Equal(0, int(clientStat.FailedBatches))
	a.Equal(0, int(clientStat.FailedLogs))
	a.NotEqual("", clientStat.LastError)
	a.Less(0, int(clientStat.LastLogs))
	a.Greater(now, clientStat.LastRetriedAt)
	a.Greater(now, clientStat.LastSucceededAt)
	a.Greater(now, clientStat.StartedAt)

	// server stat
	serverStat := server.getPoolStat(p, l)
	a.Equal(n, serverStat.Logs)
	a.Equal(n, serverStat.Contents)
	a.Equal(serverStat.Logs, int(clientStat.SentLogs))
	a.Equal(serverStat.Batches, int(clientStat.SentBatches))

	serverStat = server.getPoolStat("", "")
	a.Equal(serverStat.C200, int(clientStat.SentBatches))
	a.Equal(serverStat.C500, int(clientStat.Retried))
}

func Test_PoolNotExists(t *testing.T) {
	server.clearStat()
	a := assert.New(t)
	p := "p"
	l := "not_exists"
	n := 10000
	client := klog.NewClient(&klog.ClientOptions{
		AccessKey:           "ak",
		SecretKey:           "sk",
		Endpoint:            endpoint,
		DropIfPoolNotExists: true,
	})
	for i := 0; i < n; i++ {
		client.PushString(p, l, "message", "", "", time.Now().UnixMilli())
	}
	client.Flush(true)

	// test client stat
	clientStat := client.GetStat()
	a.Equal(n, int(clientStat.DroppedLogs))
	a.Equal(0, int(clientStat.SentLogs))
	a.Equal(3, int(clientStat.SentBatches))
}

func Test_PoolNotExistsAndNotDrop(t *testing.T) {
	server.clearStat()
	a := assert.New(t)
	p := "p"
	l := "not_exists"
	n := 10000
	client := klog.NewClient(&klog.ClientOptions{
		AccessKey:           "ak",
		SecretKey:           "sk",
		Endpoint:            endpoint,
		DropIfPoolNotExists: false,
	})
	for i := 0; i < n; i++ {
		client.PushString(p, l, "message", "", "", time.Now().UnixMilli())
	}
	client.Flush(true)

	// test client stat
	clientStat := client.GetStat()
	a.Equal(n, int(clientStat.FailedLogs))
	a.Equal(0, int(clientStat.SentLogs))
	a.Equal(3, int(clientStat.FailedBatches))
}

func Test_DownSample(t *testing.T) {
	server.clearStat()
	a := assert.New(t)
	p := "p"
	l := "l"
	n := 10000
	downSampleRate := 0.1
	client := klog.NewClient(&klog.ClientOptions{
		AccessKey:      "ak",
		SecretKey:      "sk",
		Endpoint:       endpoint,
		DownSampleRate: downSampleRate,
	})
	for i := 0; i < n; i++ {
		client.PushString(p, l, "message", "", "", time.Now().UnixMilli())
	}

	client.Flush(true)
	serverStat := server.getPoolStat(p, l)
	a.Equal(int(downSampleRate*float64(n)), serverStat.Logs)

	// test client stat
	clientStat := client.GetStat()
	a.Equal(n-serverStat.Logs, int(clientStat.DownSampledLogs))
	a.Equal(serverStat.Logs, int(clientStat.SentLogs))
}

func Test_MultiPool(t *testing.T) {
	server.clearStat()
	a := assert.New(t)
	p := "p"
	l := "l"
	poolCount := 3 // 分散到多少个日志池
	logsPerPool := 12000
	client := klog.NewClient(&klog.ClientOptions{AccessKey: "ak", SecretKey: "sk", Endpoint: endpoint})
	for i := 0; i < logsPerPool; i++ {
		for j := 0; j < poolCount; j++ {
			client.PushString(
				fmt.Sprintf("%s%d", p, j),
				fmt.Sprintf("%s%d", l, j),
				"message",
				"",
				"",
				time.Now().UnixMilli())
		}
	}

	client.Flush(true)
	for i := 0; i < poolCount; i++ {
		project := fmt.Sprintf("%s%d", p, i)
		pool := fmt.Sprintf("%s%d", l, i)
		serverStat := server.getPoolStat(project, pool)
		a.Equal(logsPerPool, serverStat.Logs)
		a.Equal(logsPerPool, serverStat.Contents)
	}

	// test client stat
	clientStat := client.GetStat()
	a.Equal(poolCount*logsPerPool, int(clientStat.SentLogs))
	a.Equal(poolCount*logsPerPool/4096+1, int(clientStat.SentBatches))
}
