package main

import (
	"encoding/json"
	"gitee.com/klogsdk/klog-go-sdk/credentials"
	"gitee.com/klogsdk/klog-go-sdk/klog"
	"log"
	"net/http"
)

func main() {
	var ak = "your access key"
	var sk = "your secret key"
	// var st = "sessionToken can be null if you don't use role accesskey"
	var st = ""
	c := klog.NewClient(&klog.ClientOptions{
		Credentials: credentials.NewStaticCredentials(ak, sk, st),
		// optional: https://klog-cn-beijing.com https://klog.api.ksyun.com
		Endpoint:   "https://klog.api.ksyun.com",
		LogLevel:   klog.LevelDebug,
		HTTPClient: http.DefaultClient,
	})

	listLogPoolReq := new(klog.ListLogPoolReq)
	listLogPoolReq.Page = 0
	listLogPoolReq.Size = 100
	listLogPoolReq.ProjectName = "ProjectName"
	listPoolResp, err := c.ListLogPools(listLogPoolReq)
	if err != nil {
		log.Println(err)
		return
	}
	bs, _ := json.Marshal(listPoolResp)
	log.Println(string(bs))
	reqId := listPoolResp.GetRequestId()
	log.Println(reqId)
}
