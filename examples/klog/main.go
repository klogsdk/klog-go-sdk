package main

import (
	"gitee.com/klogsdk/klog-go-sdk/klog"
	"time"
)

func main() {
	client := klog.NewClient(&klog.ClientOptions{
		AccessKey: "yourAccessKey",
		SecretKey: "yourSecretKey",
		Endpoint:  "http://klog-cn-beijing.ksyun.com",
		LogLevel:  klog.LevelDebug,
	})
	projectName := "yourProjectName"
	logPoolName := "yourLogPoolName"
	client.PushString(projectName, logPoolName, "hahaha1", time.Now().UnixMilli())
	client.PushString(projectName, logPoolName, "hahaha2", time.Now().UnixMilli())
	client.PushString(projectName, logPoolName, "hahaha3", time.Now().UnixMilli())

	pbLog := &klog.Log{Time: time.Now().UnixMilli()}
	pbLog.Contents = append(pbLog.Contents, &klog.Log_Content{
		Key:   "field1",
		Value: "value1",
	})
	pbLog.Contents = append(pbLog.Contents, &klog.Log_Content{
		Key:   "field2",
		Value: "value3",
	})
	pbLog.Contents = append(pbLog.Contents, &klog.Log_Content{
		Key:   "field3",
		Value: "value3",
	})
	client.Push(projectName, logPoolName, pbLog)
	// time.Sleep(time.Duration(10) * time.Second)

	client.Flush(true)

	client.PushString(projectName, logPoolName, "hahaha5", time.Now().UnixMilli())
	client.PushString(projectName, logPoolName, "hahaha6", time.Now().UnixMilli())
	client.PushString(projectName, logPoolName, "hahaha7", time.Now().UnixMilli())
	client.Flush(true)
}
